from flask import Flask, render_template
application = Flask(__name__)

@application.route("/")
def hello():
    return render_template("index.html")

@application.route("/hi")
def hi():
    return 'hola mundo'

if __name__ == "__main__":
    application.run()

